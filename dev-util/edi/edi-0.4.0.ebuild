# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit autotools

KEYWORDS="~amd64 ~arm ~x86"
SRC_URI="https://github.com/ajwillia-ms/${PN}/releases/download/v${PV}/${P}.tar.gz"

DESCRIPTION="EFL Video Player"
HOMEPAGE="https://enlightenment.org"

IUSE=""
LICENSE="BSD-2"
SLOT="0"

RDEPEND=">=dev-libs/efl-1.18"
DEPEND="${RDEPEND}"

DOCS=( AUTHORS README TODO )

src_configure() {
    ./autogen.sh || die "autogen failed"
}
