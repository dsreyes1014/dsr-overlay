# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DESCRIPTION="An IDE written in EFL"
HOMEPAGE="phab.enlightenment.org/w/projects/edi/"
LICENSE=GPLv2
SLOT=0
EGIT_REPO_URI="git://git.enlightenment.org/tools/edi.git edi-0.3.0"

RDEPENDS="
	>=dev-libs/efl-1.15.2
	>=media-libs/elementary-1.15.2
	sys-devel/clang
"

pkg_setup() {
	git-r3_fetch ${EGIT_REPO_URI}/tag/v0.3.0
}

src_prepare() {
	./autogen.sh || die "execution of ./autogen.sh in ${S} failed"
}