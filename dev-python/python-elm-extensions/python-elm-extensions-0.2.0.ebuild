# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="6"

SRC_URI="https://github.com/JeffHoogland/python-elm-extensions/archive/${PV}.tar.gz"
SLOT="0"

KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"

RDEPEND="
	>=dev-lang/python-2.7
	media-libs/elementary
	"

DEPEND="
	${RDEPEND}
	"
src_install() {
	dodir /usr/lib/python2.7/site-packages/elmextensions
	insinto /usr/lib/python2.7/site-packages/elmextensions

#	dodir /usr/lib/python3.5/site-packages/elmextensions
#	insinto /usr/lib/python3.5/site-packages/elmextensions

	doins ${S}/elmextensions/SearchableList.py
	doins ${S}/elmextensions/StandardButton.py
	doins ${S}/elmextensions/StandardPopup.py
	doins ${S}/elmextensions/__init__.py
	doins ${S}/elmextensions/aboutwindow.py
	doins ${S}/elmextensions/easythreading.py
	doins ${S}/elmextensions/embeddedterminal.py
	doins ${S}/elmextensions/fileselector.py
	doins ${S}/elmextensions/sortedlist.py
	doins ${S}/elmextensions/tabbedbox.py
}

