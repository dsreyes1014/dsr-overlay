# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6
inherit flag-o-matic qmake-utils toolchain-funcs multilib

DESCRIPTION="LV2 Noise Gate plugin"
HOMEPAGE="http://abgate.sourceforge.net/"
SRC_URI="https://sourceforge.net/projects/abgate/files/abGate-source/${P}.tar.gz/download -> ${P}.tar.gz"

LICENSE="GPL-3"
KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE=""

RDEPEND="dev-cpp/gtkmm:2.4
	dev-qt/qtcore:4
	dev-qt/qtgui:4
	|| ( media-libs/lv2 media-libs/lv2core )"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

RESTRICT="mirror"

DOCS=(README ChangeLog)

#PATCHES=("${FILESDIR}"/${P}-Makefile.patch)

src_configure() {
	cd abGateQt
	eqmake4 abGateQt.pro || die
}

src_compile() {
	PATH="$PATH:/usr/lib/qt4/bin"
	sed -i 's/qmake-qt4/qmake/g' Makefile
	sed -i 's/\-O3 /\-O3 -std=c++11 /' Makefile
	append-cxxflags $(test-flags-CXX -std=c++11) emake || die
}
