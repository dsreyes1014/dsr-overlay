# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

P="${PN}-${PV}-x86_64"
S=${WORKDIR}/${P}


DESCRIPTION="LV2 version of Linux Studio Plugins"
SRC_URI="https://sourceforge.net/projects/lsp-plugins/files/v${PV}/${P}.tar.gz/download -> ${P}.tar.gz"
KEYWORDS="alpha amd64 arm hppa ia64 ~mips ppc ppc64 sh sparc x86 ~amd64-fbsd ~x86-fbsd ~amd64-linux ~x86-linux ~ppc-macos ~x86-macos ~x86-interix ~x86-solaris"
SLOT="0"

src_install() {
    dodir /usr/$(get_libdir)/lv2
    cp -R ${S}/lsp-plugins.lv2 ${D}/usr/$(get_libdir)/lv2/ || die "cp failed"
}
