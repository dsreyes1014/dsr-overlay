# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="5"

inherit autotools base enlightenment flag-o-matic git-r3 toolchain-funcs

S=${WORKDIR}/${P}

DESCRIPTION="Web browser using EFL Webkit"
EGIT_REPO_URI="http://git.enlightenment.org/apps/eve.git"

IUSE="nls"
SLOT="0"
KEYWORDS="~x86 ~amd64"

DEPEND="net-libs/webkit-efl
	media-libs/elementary
	dev-libs/e_dbus"

src_prepare () {
	eautoreconf
}
