# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="6"

P=giada-0.12.0-src
S=${WORKDIR}/${P}
#A=giada-0.11.2-src.tar.gz

DESCRIPTION="Giada is a free, minimal, hardcore audio tool for DJs, live performers and electronic musicians."
HOMEPAGE="http://www.giadamusic.com"
SRC_URI="http://www.giadamusic.com/download/grab/source"

LICENSE="GPLv3"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
	>=x11-libs/fltk-1.3
	>=media-libs/libsndfile-1.0.25
	>=media-libs/libsamplerate-0.1.8
	>=dev-libs/jansson-2.7
	>=media-libs/rtmidi-2.1.0
	"

src_unpack() {
	#A=giada-0.11.2-src.tar.gz
	#unpack=${A}
	tar -zxf ${DISTDIR}/${A}
}

src_configure() {

	export LDFLAGS="-L/usr/$(get_libdir)/fltk ${LDFLAGS}"
	export CPPFLAGS="-I/usr/include/fltk ${CPPFLAGS}"

	econf \
		--target=linux
}
