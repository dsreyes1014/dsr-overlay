# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="6"

PYTHON_COMPAT=( python2_7 python3_5 )
PYTHON_REQ_USE="threads(+)"

inherit python-r1 waf-utils

DESCRIPTION="A simple Linux Guitar Amplifier for jack with one input and two outputs"
HOMEPAGE="http://guitarix.sourceforge.net/"

SRC_URI="https://sourceforge.net/projects/guitarix/files/guitarix/${P}.tar.xz/download -> ${P}.tar.xz"
KEYWORDS="~amd64 ~x86"

SLOT="0"
LICENSE="GPL-2"

IUSE="avahi bluez debug faust ladspa lv2 meterbridge nls"

RDEPEND="
	media-libs/ladspa-sdk
	=x11-libs/gtk+-2*
	=dev-cpp/gtkmm-2*
	>=dev-libs/glib-2.10
	>=dev-libs/libsigc++-2.0
	media-libs/zita-convolver
	media-libs/zita-resampler
	>=media-libs/libsndfile-1.0.17
	>=media-sound/jack-audio-connection-kit-0.109.1
	>=dev-libs/boost-1.38
	bluez? ( net-wireless/bluez )
	meterbridge? ( media-sound/meterbridge )
	avahi? ( net-dns/avahi )
	nls? ( dev-util/intltool )
	net-libs/webkit-gtk
	dev-cpp/eigen
    media-libs/liblrdf
    media-libs/lilv
"

DEPEND="
	${RDEPEND}
	virtual/pkgconfig
"

DOCS=( changelog README )

src_unpack() {
	tar -Jxf ${DISTDIR}/${P}.tar.xz
}

PN="guitarix"
P=${PN}-${PV}
S=${WORKDIR}/${P}

src_configure() {
	python_setup 'python2*'

	waf-utils_src_configure \
		--cxxflags-debug="" \
		--cxxflags-release="-DNDEBUG" \
		--nocache \
		--shared-lib \
		--lib-dev \
		--no-ldconfig \
		--no-desktop-update \
		$(use_enable nls) \
		$(usex avahi "" --no-avahi) \
		$(usex debug --debug "") \
		$(usex faust --faust --no-faust) \
		$(usex ladspa --ladspadir="${EPREFIX}"/usr/share/ladspa "--no-ladspa --no-new-ladspa") \
		$(usex lv2 --lv2dir="${EPREFIX}"/usr/$(get_libdir)/lv2 --no-lv2) \
		$(usex bluez ""--no-bluez)
}
