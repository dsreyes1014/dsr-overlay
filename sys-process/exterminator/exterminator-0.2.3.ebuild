# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="6"

PN="Exterminator"
P="${PN}-${PV}"
S="${WORKDIR}/${P}"

DESCRIPTION="System process killer written in Python and Elementary"
SRC_URI="https://github.com/JeffHoogland/Exterminator/archive/${PV}.tar.gz"

SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"

RDEPEND="
	>=dev-lang/python-3.5 
	>=dev-python/python-elm-extensions-0.2.0
	>=dev-python/psutil-2.1.3
	>=dev-python/python-efl-1.12
	"
DEPEND="
	${RDEPEND}
"

src_install() {
	dobin ${S}/exterminator
	insinto /usr/share/applications
	doins ${S}/exterminator.desktop
}
