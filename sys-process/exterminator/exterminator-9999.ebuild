# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="5"

inherit autotools base flag-o-matic toolchain-funcs

S="${WORKDIR}"

DESCRIPTION="System process killer written in Python and Elementary"
SRC_URI="https://github.com/JeffHoogland/Exterminator/tarball/master/JeffHoogland-Exterminator-0.2.3-4-gfd4614e.tar.gz"

SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"

IUSE="psutil python"

RDEPEND="
	python? ( dev-lang/python )
	psutil? ( dev-python/psutil )
	"
src_unpack() {
	unpack ${A}
	cd "${S}"
}

src_install() {
	ls ./
	cd *
	ls ./
	dobin ./exterminator
	insinto /usr/share/applications
	doins exterminator.desktop
}
