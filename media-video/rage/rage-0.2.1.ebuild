# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6
KEYWORDS="~amd64 ~arm ~x86"
SRC_URI="https://download.enlightenment.org/rel/apps/${PN}/${PN}-${PV}.tar.gz"

DESCRIPTION="EFL Video Player"
HOMEPAGE="https://enlightenment.org"

IUSE=""
LICENSE="BSD-2"
SLOT="0"

RDEPEND=">=dev-libs/efl-1.18"
DEPEND="${RDEPEND}"

DOCS=( AUTHORS README TODO )
