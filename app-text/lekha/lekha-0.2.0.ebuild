# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="6"

PYTHON_COMPAT=( python2_7 python3_5 )

inherit distutils-r1

DESCRIPTION="A pdf viewer written in EFL and Python"
LICENSE="GPLv3"
SRC_URI="https://github.com/JeffHoogland/lekha/archive/v${PV}.tar.gz"
SLOT="0"

KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"

RDEPEND="
	>=dev-python/python-efl-1.14	   
	dev-python/PyPDF2
	dev-python/pyxdg
	dev-python/python-elm-extensions	
"

DEPEND="${RDEPEND}"

src_install() {
	esetup.py install --root=${D}
}