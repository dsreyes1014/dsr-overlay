# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

PYTHON_COMPAT=( python3_6 )

inherit distutils-r1

PN="OpenLP"
P=${PN}-${PV}
S=${WORKDIR}/${P}

DESCRIPTION="An open-source presentation platform created for churches
large and small"

HOMEPAGE="www.openlp.org"
LICENSE="GPLv2"
SRC_URI="https://get.openlp.org/${PV}/${P}.tar.gz"

KEYWORDS="~amd64"
SLOT="0"
IUSE="vlc mupdf"

RDEPEND="dev-python/chardet
	dev-python/pyenchant
	dev-python/lxml
	dev-python/mako
	dev-python/alembic
	dev-python/PyQt5[gles2,dbus,gui,multimedia,opengl,network,sql,svg,testlib,webkit,widgets]
	dev-python/dbus-python
	dev-python/beautifulsoup
	dev-python/sqlalchemy
	dev-python/nose
	dev-python/mock
	mupdf? ( >=app-text/mupdf-1.8 )
	vlc? ( >=media-video/vlc-2.2.1 )
	"

#     dev-qt/qtmultimedia[gles2,gstreamer,alsa,pulseaudio,widgets]
#	dev-qt/qtwebkit[multimedia,opengl,webp,printsupport]
#	dev-qt/qtwidgets[gles2,png]
#	dev-qt/qtgui[egl,libinput,tslib,ibus,gles2,dbus,jpeg,png,udev,gif]
#	dev-qt/qtopengl[gles2]

DEPEND="${RDEPEND}
	>=dev-lang/python-3.6
	"

src_install() {
	cd ${S}

	python_export python3.6 EPYTHON
	esetup.py install --root=${D} --optimize=1

	mv -v ${D}/usr/bin/openlp.py ${D}/usr/bin/openlp

	# install desktop file
	insinto /usr/share/applications
	doins resources/openlp.desktop

	# install icon
	insinto /usr/share/pixmaps
	newins resources/images/openlp-logo.svg openlp.svg
	insinto /usr/share/pixmaps
	newins resources/images/openlp-logo-48x48.png openlp.png
}
