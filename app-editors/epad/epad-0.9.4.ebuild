# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

PN="ePad"
P="${PN}-${PV}"
S="${WORKDIR}/${P}"

SRC_URI="https://github.com/JeffHoogland/ePad/archive/${PV}.tar.gz"
SLOT="0"

KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"

RDEPEND="
	>=dev-lang/python-2.7
	>=dev-python/python-efl-1.12
	>=dev-python/python-elm-extensions-0.2.0
	media-libs/elementary
"

DEPEND="
        ${RDEPEND}
"
src_install() {
	dobin ${S}/epad
	insinto /usr/share/applications
	doins ${S}/ePad.desktop
}
