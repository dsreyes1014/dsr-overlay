# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit autotools

SRC_URI="http://download.enlightenment.org/rel/apps/${PN}/${P}.tar.gz"
SLOT="0"

KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"

RDEPEND="
	>=dev-libs/efl-1.18
"

DEPEND="
        ${RDEPEND}
"
src_configure() {
	./autogen.sh || die "autogen failed"
}
